package com.oglofus.gringotts.sponge

import com.google.inject.Inject
import com.oglofus.gringotts.api.GringottsPlugin
import org.slf4j.Logger
import org.spongepowered.api.event.Listener
import org.spongepowered.api.event.game.state.GameLoadCompleteEvent
import org.spongepowered.api.event.game.state.GameStartedServerEvent
import org.spongepowered.api.plugin.Plugin

@Plugin(id = "gringotts", authors = ["Nikolaos Grammatikos"], name = "GringottsPlugin", version = "1.0.1-SNAPSHOT")
class GringottsSponge : GringottsPlugin {
    @Inject
    private lateinit var logger: Logger

    @Listener
    fun onGameLoadComplete(event: GameLoadCompleteEvent) {
    }

    @Listener
    fun onServerStart(event: GameStartedServerEvent) {
    }
}
