package com.oglofus.gringotts.api.service

import com.oglofus.gringotts.api.Account
import java.util.*

interface AccountService : Service<UUID, Account> {
}
