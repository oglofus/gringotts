package com.oglofus.gringotts.api

interface Account : Identifiable {
    val balance: Long
}
