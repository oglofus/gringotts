package com.oglofus.gringotts.api.service

import java.nio.file.Path

interface Service<K, T> : Map<K, T> {
    val path: Path

    fun save()

    fun load()
}
