package com.oglofus.gringotts.api

import com.oglofus.gringotts.api.service.AccountService

interface GringottsPlugin {
    val accounts: AccountService
}
