package com.oglofus.gringotts.api

import java.util.*

interface Identifiable {
    public val uuid: UUID
}
