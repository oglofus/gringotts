package com.oglofus.gringotts.bukkit

import com.oglofus.gringotts.api.GringottsPlugin
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin

@Suppress("unused", "RedundantVisibilityModifier")
class GringottsBukkit : JavaPlugin(), GringottsPlugin {
    override fun onEnable() {
    }
}
